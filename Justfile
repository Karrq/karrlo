export RUST_BACKTRACE := "full"

# Show this menu
@help:
    just --list --unsorted
    cargo run -- --help

run:
    cargo run
