use anyhow::{bail, Result};
use rustix::{fd::AsFd, io};

mod raw_mode;
use raw_mode::RawMode;

fn is_ascii_control(c: u8) -> bool {
    !(c > 32 && c < 126)
}

const fn control_key(c: u8) -> u8 {
    c & 0x1f
}

fn read_key(fd: impl AsFd) -> Result<u8> {
    let mut buf = [0];

    while io::read(&fd, &mut buf)? != 1 {}

    Ok(buf[0])
}

fn process_key(fd: impl AsFd) -> Result<()> {
    let key = read_key(&fd)?;

    match key {
        c if c == control_key(b'q') => bail!("Quit program"),
        c if is_ascii_control(c) => println!("{}\r", c),
        c => println!("{} ('{}')\r", c, std::str::from_utf8(&[c]).unwrap()),
    }

    Ok(())
}

pub fn run(fd: impl AsFd) -> Result<()> {
    let raw = RawMode::new(&fd)?;
    raw.enable_raw_mode()?;

    let err;
    loop {
        match process_key(&fd) {
            Ok(_) => continue,
            Err(e) => {
                err = Err(e);
                break;
            }
        }
    }

    raw.restore()?;
    err
}
