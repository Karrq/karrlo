use anyhow::Result;
use rustix::{
    fd::AsFd,
    termios::{
        self, OptionalActions, Termios, BRKINT, CS8, ECHO, ICANON, ICRNL, IEXTEN, INPCK, ISIG,
        ISTRIP, IXON, OPOST, VMIN, VTIME,
    },
};

/// Handler to facilitate enabling of raw mode
pub struct RawMode<T> {
    prev: Termios,
    fd: T,
}

impl<T: AsFd> RawMode<T> {
    /// Create a new RawMode handler for the given file descriptor
    pub fn new(fd: T) -> Result<Self> {
        let prev = termios::tcgetattr(&fd)?;

        Ok(Self { prev, fd })
    }

    /// Enable raw mode
    ///
    /// This method will enable raw mode for the given file descriptor if possible
    pub fn enable_raw_mode(&self) -> Result<()> {
        let mut termios = self.prev.clone();

        termios.c_iflag &= !(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
        termios.c_oflag &= !(OPOST);
        termios.c_cflag |= CS8;
        termios.c_lflag &= !(ECHO | ICANON | IEXTEN | ISIG);

        termios.c_cc[VMIN] = 0;
        termios.c_cc[VTIME] = 1;

        termios::tcsetattr(&self.fd, OptionalActions::Flush, &termios)?;

        Ok(())
    }

    /// Restores the original configuration
    ///
    /// This method will attempt to restore the original coniguration of the
    /// given file descriptor
    pub fn restore(&self) -> Result<()> {
        termios::tcsetattr(&self.fd, OptionalActions::Flush, &self.prev)?;
        Ok(())
    }
}
