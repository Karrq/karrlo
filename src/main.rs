use anyhow::Result;

pub fn main() -> Result<()> {
    let stdin = unsafe { rustix::io::take_stdin() };

    karrlo::run(stdin)
}
